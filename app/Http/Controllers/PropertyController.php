<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;
use App\PropertyReview;

class PropertyController extends Controller
{
    public function viewProperty($property_id) {
        $property = Property::find($property_id);
        return view('view-property', compact('property'));
    }
    public function newReview() {
        $req = request();
        $args = $req->validate([
            'rating' => 'required|integer',
            'property_id' => 'required|integer',
            'comment' => ''
        ]);
        $propertyReview = new PropertyReview;
        foreach($args as $key => $value) {
            $propertyReview->{$key} = $value;
        }
        $propertyReview->save();
        //issue redirect back to property page to ensure that refresh won't create new entry
        return redirect("/property/{$args['property_id']}");
    }
    public function getPropertyList() {
        $filterBy = request()->get('filterBy');
        
        //grab properties with propertyReviews and propertyTypes for the below
        $properties = Property
            ::with('PropertyReviews')
            ->with('PropertyType');

        switch(request()->get('sortBy')) {
            //sort by alpha
            case 'alpha':
                $properties = $properties
                    ->get()
                    ->sortBy('title');
                break;
            //sort by alpha in reverse
            case 'alpha-reverse':
                $properties = $properties
                    ->get()
                    ->sortBy('title')
                    ->reverse();
                break;
            case 'rating':
            //sort by rating
                $properties = $properties
                    ->get();
                foreach($properties as $property) {
                    $reviews = $property->PropertyReviews->pluck('rating');
                    $property->averageRating = (
                       $reviews->count()
                       ? ($reviews->sum() / $reviews->count())
                       : 0
                   ); 
                }
                $properties = $properties->sortBy('averageRating');
                break;
            case 'rating-reverse':
            //sort by rating in reverse
                $properties = $properties
                    ->get();
                foreach($properties as $property) {
                    $reviews = $property->PropertyReviews->pluck('rating');
                    $property->averageRating = (
                       $reviews->count()
                       ? $reviews->sum() / $reviews->count()
                       : 0
                   ); 
                }
                $properties = $properties->sortBy('averageRating')->reverse();
                break;
        }
        if($filterBy && $filterBy != 'all') {
            //if filterBy isn't all is a parameter, then filter by propertyType
            $properties = $properties->filter(function($property) use ($filterBy) {
                return (str_slug($property->PropertyType->title) == str_slug($filterBy));
            });
        }
        return view('/inc/property-list', compact('properties'));
    }
}
