<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = 'property';
    protected $primaryKey = 'property_id';
    
    /** relationships **/
    public function PropertyType() {
        return $this->belongsTo('App\PropertyType', 'property_type_id', 'property_type_id');
    }
    public function PropertyReviews() {
        return $this->hasMany('App\PropertyReview', 'property_id', 'property_id');
    }
    /** end relationships **/
}
