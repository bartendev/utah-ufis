<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyReview extends Model
{
    //
    protected $table = 'property_review';
    protected $primaryKey = 'property_review_id';

}
