$(function(){
    var sortBy = 'alpha';
    var filterBy = 'all';
    function sortFilterProperties () {
        $.ajax('/properties/get', {
            data: {
                sortBy: sortBy,
                filterBy: filterBy
            },
            success: function(data) {
                $('.links').html(data);
                var ri = $('.rateit');
                ri.each(function(i, e) {
                    var $e = $(e);
                    console.log($e.attr('data-rateit-value'));
                    $e.rateit({value:  $e.attr('data-rateit-value'), step: .1, readonly: true});
                });
            }
        });
    }
    $('.type-filters [data-filter]').on('click touch', function(e) {
        var $e = $(e.target);
        filterBy = $e.attr('data-filter');
        $('.type-filters [data-filter]').removeClass('active');
        $e.addClass('active');
        sortFilterProperties();
    });
    $('.propertySortBy .sortBy').on('click touch', function(e) {
        var $e = $(e.target);
        $e.parents('.propertySortBy').find('.sortByButton').text(
            $e.text()
        );
        sortBy = $e.attr('data-sortby');
        sortFilterProperties();
    });
});

