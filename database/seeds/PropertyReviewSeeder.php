<?php

use Illuminate\Database\Seeder;
use App\PropertyReview;

class PropertyReviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($counter = 1; $counter <= 5; $counter+=1) {
            factory(PropertyReview::class, $counter)
                ->create([
                    'property_id' => $counter
                ]);
        }
    }
}
