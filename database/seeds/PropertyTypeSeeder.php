<?php

use Illuminate\Database\Seeder;
use App\PropertyType;
use App\Property;

class PropertyTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            'House' => [1, 2],
            'Apartment' => [3, 4],
            'Cabin' => [5]
        ];
        $properties = Property::all()
            ->keyBy('property_id');
        foreach($types as $type => $property_ids){
            $propertyType = new PropertyType;
            $propertyType->title = $type;
            $propertyType->save();
            foreach($property_ids as $property_id){
                $property = $properties[$property_id];
                $property->property_type_id = $propertyType->property_type_id;
                $property->save();
            }
        }
    }
}
