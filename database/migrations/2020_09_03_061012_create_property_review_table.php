<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_review', function (Blueprint $table) {
            $table->bigIncrements('property_review_id');
            $table->integer('property_id');
            $table->integer('rating');
            $table->string('comment')->nullable();
            $table->timestamps();
            $table->index('property_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_review');
    }
}
