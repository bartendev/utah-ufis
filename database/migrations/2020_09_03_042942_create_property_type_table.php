<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //creating seperate table to keep track of property types
        Schema::create('property_type', function (Blueprint $table) {
            $table->increments('property_type_id');
            $table->string('title');
            $table->timestamps();
        });
        //allow belongsTo relation
        Schema::table('property', function (Blueprint $table) {
            $table->integer('property_type_id')->nullable();
            $table->index('property_type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_type');
        Schema::table('property', function (Blueprint $table) {
            $table->dropColumn('property_type_id');
        });
    }
}
