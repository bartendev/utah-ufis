<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\PropertyReview;

use App\Model;
use Faker\Generator as Faker;

$factory->define(PropertyReview::class, function (Faker $faker) {
    return [
        'rating' => $faker->numberBetween(1, 5),
        'comment' => $faker->text(200),
        'property_id' => $faker->numberBetween(1, 5)
    ];
});
