<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Property;

class PropertyTypeTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testPropertyTypeName()
    {
        $properties = Property::all();
        
        foreach($properties as $property) {
            if ( in_array($property->property_id, [1,2]) ) {
                $this->assertTrue($property->PropertyType->title == "House");
            } elseif( in_array($property->property_id, [3,4]) ) {
                $this->assertTrue($property->PropertyType->title == "Apartment");
            } elseif( in_array($property->property_id, [5]) ) {
                $this->assertTrue($property->PropertyType->title == "Cabin");
            }
        }
    }
}
