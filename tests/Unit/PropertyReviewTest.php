<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Property;

class PropertyReviewTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testReviews()
    {
        //ensure proper numbers of reviews and that review rating is between 1 - 5
        $properties = Property::all();
        foreach($properties as $property) {
            $this->assertTrue(
                $property->PropertyReviews->count() >= $property->property_id
            );
            foreach($property->PropertyReviews as $review) {
                $this->assertTrue($review->rating <= 5 && $review->rating >= 1); 
            }
        }
    }
}
