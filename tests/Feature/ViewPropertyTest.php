<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ViewPropertyTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testProperty()
    {
        $response = $this->get('/property/1');
        $response->assertStatus(200);
        $response->assertSeeText('Heart of Salt Lake City');
    }
}
