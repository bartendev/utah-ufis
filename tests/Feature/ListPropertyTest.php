<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ListPropertyTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        //this test was added after the fact as I had originally made the filters using just jquery on the client, but realized by pushing this to the server I could test it
        $response = $this->get('/properties/get?sortBy=alpha&filterBy=house');
        $response->assertStatus(200);
        $response = $this->get('/properties/get?sortBy=alpha-reverse&filterBy=apartment');
        $response->assertStatus(200);
        $response = $this->get('/properties/get?sortBy=rating&filterBy=apartment');
        $response->assertStatus(200);
        $response = $this->get('/properties/get?sortBy=rating-reverse&filterBy=cabin');
        $response->assertStatus(200);
    }
}
