<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\PropertyReview;

class createReviewTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $testParams = [
            'property_id' => 1,	
            'rating' => 1,	
            'comment' => 'This is a fictiious review compleeete with unbeleivable speellings',	
        ];
        if(
            $review = PropertyReview
            ::where($testParams)
            ->first()
        ) {
            $review->delete();
        }
        $response = $this->post('/reviews', [
            $testParams
        ]);
        $response->assertStatus(302);
        if(
            $review = PropertyReview
            ::where($testParams)
            ->first()
        ) {
            $review->delete();
        }
        
    }
}
