@include('inc/header', ['title' => 'Welcome to ' . $property->title])
        <div class="full-height">
            <div class="container">
                <div class="content">
                <h1 class="title m-b-md h2">
                    Welcome to {{ $property->title }}
                </h1>

                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Property</li>
                    <li class="breadcrumb-item active" aria-current="page">{{ $property->title }}</li>
                  </ol>
                </nav>
                <div><strong>PropertyType: </strong>{{ $property->PropertyType->title }}</div>
                <div class="links">
                    @foreach($property->PropertyReviews as $propertyReview)
                        <div class="card">
                            <div class="card-body row text-left">
                                <div class="col-12">
                                    <div class="rateit" data-rateit-value="{{ $propertyReview->rating }}" data-rateit-ispreset="true" data-rateit-readonly="true"></div>
                                </div>
                                <div class="col-12"><strong>Comment: </strong>{{ $propertyReview->comment }}</div>
                            </div>
                        </div>
                    @endforeach
                    <div class="container pt-5">
                        <form method="POST" action="/reviews">
                            {{ csrf_field() }}
                            <h2>Create New Review</h2>
                            <input type="hidden" name="property_id" value="<?=$property->property_id;?>">
                            <select id="rating" name="rating">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <div class="rateit" data-rateit-backingfld="#rating" ></div>
                            <textarea class="form-control" name="comment" placeholder="Enter Review Comment"></textarea>
                            <input class="btn btn-primary" type="submit" value="New Review">
                        </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
@include('inc/footer')

