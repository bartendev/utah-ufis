@include('inc/header')
        <div class="container">
<div class="full-height">
            <div class="content">
                <h1 class="title m-b-md h2">
                    Welcome to UFIS-BNB!
                </h1>

                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                  </ol>
                </nav>
                <div class="type-filters">
                    <a class="btn btn-default active" href="#" data-filter="">All</a>
@foreach($properties->pluck('PropertyType.title')->unique() as $type)
                    <a class="btn btn-default" href="#" data-filter="{{ str_slug($type) }}">{{ $type }}</a>
@endforeach
                </div>
                <div class="dropdown propertySortBy">
                  <button class="btn btn-secondary dropdown-toggle sortByButton" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Sort By Alphabetical
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item sortBy" data-sortby="alpha" href="#">Sort By Alphabetical</a>
                    <a class="dropdown-item sortBy" data-sortby="alpha-reverse" href="#">Sort By Alphabetical Reverse</a>
                    <a class="dropdown-item sortBy" data-sortby="rating" href="#">Sort By Ranking</a>
                    <a class="dropdown-item sortBy" data-sortby="rating-reverse" href="#">Sort By Rating Reverse</a>
                  </div>
                </div>
                <div class="links type-filtered">
@include('inc/property-list', ['properties' => $properties])
                </div>
            </div>
        </div></div>
@include('inc/footer')
