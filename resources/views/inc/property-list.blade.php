@foreach($properties as $property)
<?php
    $ratings = $property->PropertyReviews->pluck('rating');
    $avg = $ratings->sum() / $ratings->count();
?>
<a data-type="{{ str_slug($property->PropertyType->title) }}" href="/property/{{ $property->property_id }}">{{ $property->title }} 
        <span class="rateit" data-rateit-step=".1" data-rateit-value="{{ $avg }}" data-rateit-ispreset="true" data-rateit-readonly="true"></span>
</a>
@endforeach
